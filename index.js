// alert("h");


const getCube = ((number) => {
	let x = number ** 3;
	console.log(`The cube if ${number} is ${x}`)
});

getCube(2);



const address = [1218, "General Trias", "Cavite", 4107];
const [houseNumber, province, city, zipCode] = address;
console.log(`I Live at ${houseNumber} ${province} ${city} ${zipCode}`);




const animal = {
	name: "Lolong",
	animalType: " saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
};

const {name, animalType, weight, measurement} =  animal;
console.log(`${name} was a ${animalType}. He weighed at ${weight} with a measurement of ${measurement}.`)




const numbers = [1, 2, 3, 4, 5, 15];

numbers.forEach((number) => console.log(number));




class Dog{
	constructor(name, age, breed){
		this.name = name
		this.age = age
		this.breed = breed
	}
}

const myPet = new Dog("Frankie", 5, "Miniature Dachshund");
console.log(myPet)


